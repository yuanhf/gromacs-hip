# 东方超算系统GROMACS使用指南

文档版本v1.0，2023年8月19日，徐顺， xushun@sccas.cn

本指南主要面向基于C86平台的**东方超算系统**用户，也可作为其他平台用户参考。C86平台的CPU处理器兼容X86指令集，类GPU处理器DCU采用兼容AMD ROCM的DTK软件开发与运行环境。

# 一、概要

GROMACS是当前主流的开源分子动力学模拟程序之一，最早由格罗宁根大学（Groningen University）开发，目前的领导团队属于斯德哥尔摩大学（Stockholm University）。GROMACS主要用来模拟蛋白质和脂质这样的生物分子，也可以用来研究诸如聚合物等非生物分子体系，在生物大分子以及非生物有机大分子等系统研究中都有广泛的应用。GROMACS支持目前常用的分子动力学算法，包括各种热浴、压浴以及静电相互作用计算等，还包含模拟体系构建工具和前后处理分析工具在内的大量实用程序。与其它分子动力学软件相比，其优势主要表现在计算速度快，CPU版本核心计算代码都由汇编语言写成；支持基于MPI的并行计算；通过CUDA支持NVIDIA GPU计算，通过OpenCL支持其他加速卡的计算加速。

- 官方网站: http://www.gromacs.org
- 代码库: https://gitlab.com/gromacs/gromacs.git
- 使用指南 http://manual.gromacs.org
  - 安装说明  https://manual.gromacs.org/current/install-guide/index.html
  - 开发指南  https://manual.gromacs.org/current/dev-manual/index.html

- tutorials 官方 https://www.gromacs.org/tutorial_webinar.html
- tutorials 2018非官方  http://www.mdtutorials.com/gmx/index.html
- 入门教程--三种典型系统(ala10, argon, water) 的模拟
  - https://extras.csc.fi/chem/courses/gmx2004/, [exercises-answersk](https://extras.csc.fi/chem/courses/gmx2004/exercises/answers.html)
  - https://extras.csc.fi/chem/courses/gmx2007/ 
- 高级教程 [Advanced GROMACS workshop @ CSC 2021](https://events.prace-ri.eu/event/1148/) , [Advaced_topics](https://a3s.fi/advanced_gmx/PRACE_CSC_BioExcelWorkshop-GROMACS_workflows_and_advanced_topics.html)

# 二、源代码说明

GROMACS软件会发布一些稳定性的源代码版本，下载地址https://manual.gromacs.org/current/download.html 也可以去[github库]( https://gitlab.com/gromacs/gromacs.git)下载最新源代码。下载并解压最新发布版的源代码包

```bash
wget https://ftp.gromacs.org/gromacs/gromacs-2023.2.tar.gz
tar xzvf gromacs-2023.2.tar.gz
```

GROMACS 2023.2版本的源代码中包含以下目录和文件：

- [src](https://gitlab.com/gromacs/gromacs/-/tree/main/src)  主要实现程序和gmx框架
- [api](https://gitlab.com/gromacs/gromacs/-/tree/main/api)  提供gmxapi和nblib库
- [python_packaging](https://gitlab.com/gromacs/gromacs/-/tree/main/python_packaging)  gmxapi较独立的Python模块，及其测试脚本
- [scripts](https://gitlab.com/gromacs/gromacs/-/tree/main/scripts) shell环境配置文件GMXRC和xplor2gmx工具脚本
- [share](https://gitlab.com/gromacs/gromacs/-/tree/main/share) 分子拓扑文件及基于拓扑文件的用户分析模板
- [cmake](https://gitlab.com/gromacs/gromacs/-/tree/main/cmake)  CMake编译相关文件
- [docs](https://gitlab.com/gromacs/gromacs/-/tree/main/docs) 使用文档原始文件，对应官方[在线版](http://manual.gromacs.org/documentation)，也可以生成编译pdf离线版
- [tests](https://gitlab.com/gromacs/gromacs/-/tree/main/tests)  基于regressiontests的测试接口
- [admin](https://gitlab.com/gromacs/gromacs/-/tree/main/admin)  各种代码管理相关命令脚本
- [INSTALL](https://gitlab.com/gromacs/gromacs/-/tree/main/INSTALL-dev) 安装说明
- [README](https://gitlab.com/gromacs/gromacs/-/tree/main/README) 说明简介

GROMACS当前以年份为版本代号，相对之前5.x序列代码进行了C到C++的重整。

# 三、软件安装

GROMACS软件提供了较多的编译选项，用户可以根据需要设置。在东方超算系统中，已提供编译的GROMACS版本，使用以下命令加载：

```
module load apps/gromacs-DCU/2020.3/hpcx-v2.7.4-gcc-7.3.1
```

可执行文件为gmx_mpi。一般用户可跳过“**软件安装”**这节，直接转到“**实例测试**”。

用户在某些特殊场合下需要重新编译GROMACS，如需要使用最新的版本，或者需要GPU加速版本和双精度版本等等，这时用户就需要自己编译GROMACS软件。GROMACS编译方式主要的配置：

- 提供单精度single和双精度double两种浮点精度计算方式
- 提供标准MPI、Thread MPI和OpenMP多线程的并行计算方式
- 提供NVIDIA GPU、SYCL、OpenCL等可选加速计算方式
- 提供不同位长的矢量化（SSE、AVX）指令加速
- 支持Python模块的编译方式

GROMACS默认的安装命令为

```bash
tar xfz gromacs-2023.2.tar.gz
cd gromacs-2023.2
mkdir build
cd build
cmake .. -DGMX_BUILD_OWN_FFTW=ON -DREGRESSIONTEST_DOWNLOAD=ON
make
make check
sudo make install
source /usr/local/gromacs/bin/GMXRC
```

但以上命令中cmake配置有各种依赖，如GROMACS 2023.2版本需要的编译工具有 CMake 3.18.4及以上, Python 3.7和GCC 9以支持C++17，需要MPI最好有HWLOC软件包安装，如OpenMPI软件工具。我们先安装一些依赖库。

**安装FFTW3库**

GROMACS 2020 开启`-DGMX_BUILD_OWN_FFTW=ON`选项自动编译fftw3时，会自动下载fftw-3.3.8版本，并使用--enable-sse2 --enable-avx --enable-avx2选项编译。
我们可以手动安装FFTW3，这样可以为GROMACS不同的编译版本共享fftw3编译库。

```
wget https://fftw.org/pub/fftw/fftw-3.3.8.tar.gz
```

默认编译双精度，--enable-shared除了默认静态库，还需要编译so动态库

```bash
./configure --prefix=/opt/soft/fftw-3.3.8 --enable-sse2 --enable-avx --enable-avx2 --enable-shared
make -j8
make install
make clean
```

再次编译单精度，因为FFTW单精度编译不会和双精度编译安装文件名冲突，因此推荐安装在同一个目录，这里为/opt/soft/fftw-3.3.8

```bash
./configure --prefix=/opt/soft/fftw-3.3.8 --enable-sse2 --enable-avx --enable-avx2 --enable-shared  --enable-float
```

使用脚本create_module_file.sh创建一个module file。

```bash
[root@mu01 ~]# source /opt/soft/src/create_module_file.sh
[root@mu01 ~]# create_module_file /opt/soft/fftw-3.3.8 fftw-3.3.8 /opt/soft/modules-tcl/modulefiles/library/fftw3/3.3.8
```

module文件实例

```
[root@mu01 ~]# cat /opt/soft/modules-tcl/modulefiles/library/fftw3/3.3.8
#%Module

set name "fftw"
set ver "3.3.8"
set kern [ exec uname -s ]
module-whatis "Name        : fftw"
module-whatis "Version     : 3.3.8"
module-whatis "Description : fftw-3.3.8 for $kern"
set base_dir  /opt/soft/fftw-3.3.8
setenv FFTW_ROOT  $base_dir
prepend-path  PATH   $base_dir/bin
prepend-path LD_LIBRARY_PATH $base_dir/lib
prepend-path  MANPATH   $base_dir/share/man
prepend-path  INFOPATH   $base_dir/share/info
prepend-path  PKG_CONFIG_PATH   $base_dir/lib/pkgconfig
```

注意GROMACS编译时，通过pkgconfig来定位fftw库的位置，因此这里需要正确设置PKG_CONFIG_PATH 指向fftw3库提供的pkgconfig文件。

**安装HWLOC库**

```
wget https://download.open-mpi.org/release/hwloc/v2.9/hwloc-2.9.2.tar.gz
```

**编译GROMACS**

GROMACS提供CUDA、SYCL和OpenCL等三种GPU加速计算方式。在C86平台可以使用OpenCL加速，也可以使用HIP加速方式。如果基于OpenCL的编译，可以使用CUDA SDK中自带的[OpenCL](https://developer.nvidia.com/opencl)，也可以使用AMD ROCm SDK中提供的OpenCL。在**C86平台推荐使用GPU package的HIP加速方式**，比OpenCL加速方式大约有3%的速度提升。需要注意CUDA 10.2需要GCC版本不超过8.x，因此这里使用GCC 8.3.0版本。在关闭GMX_MPI时，会开启GMX_THREAD_MPI，两种二选一。

在运行cmake之前，需要加载好编译环境

```bash
module purge 
module load compiler/devtoolset/9.3.1
module load compiler/cmake/3.24.1
module load compiler/rocm/dtk/23.04
module load mpi/openmpi/4.1.5-gcc9.3.0
module load mathlib/openblas-0.3.15
module load apps/anaconda3/2022.10
source activate py3q
```

编译选项有可能有修改，如

```bash
mkdir build
cd build
cmake -L -DCMAKE_INSTALL_PREFIX=$HOME/local/gromacs-2023.2 -DGMX_HWLOC=ON -DGMX_MPI=ON -DGMX_BUILD_OWN_FFTW=ON -DREGRESSIONTEST_DOWNLOAD=OFF ..
cmake --build . --parallel 6
cmake --install .
```

通过cmake命令可以给定安装目录。

**编译单节点版本**

```bash
module add compiler/cmake/3.10.1 compiler/gcc/8.3.0 library/fftw3/3.3.8
cmake -L -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/opt/soft/gromacs-2020.3_tmpi-cu04 -DGMX_BUILD_OWN_FFTW=OFF -DGMX_GPU=CUDA -DGMX_MPI=OFF ..
```

添加HWLOC支持的版本

HWLOC库原属于OpenMPI的使用库，后来独立出来开发，GROMACS使用HWLOC获取硬件（处理器，内存等）的配置信息，便于自动优化运行参数。

```bash
module add compiler/cmake/3.10.1 compiler/gcc/8.3.0 library/fftw3/3.3.8 
module add library/hwloc/2.9.2
cmake -L -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/opt/soft/gromacs-2020.3_tmpi-hwloc-cu04 -DGMX_BUILD_OWN_FFTW=OFF -DHWLOC_DIR=/opt/soft/hwloc-2.9.2 -DGMX_GPU=CUDA -DGMX_MPI=OFF ..
```

**编译MPI跨节点版本**

```bash
module add compiler/openmpi/4.1.5_GNU8.3.0
cmake -L -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/opt/soft/gromacs-2020.3_mpi-cu04 -DGMX_BUILD_OWN_FFTW=ON -DGMX_GPU=CUDA -DGMX_MPI=ON ..
```



# 四、实例测试

## 配置说明

在东方超算系统中，已经提供安装好的GROMACS版本，通过module加载相应的版本，这里加载GROMACS 2020.3的DCU加速的版本：

```bash
module rm compiler/devtoolset
module rm compiler/rocm/dtk
module rm mpi/hpcx
module add compiler/devtoolset/7.3.1
module add compiler/rocm/dtk/21.04
module add mpi/hpcx/2.7.4/gcc-7.3.1
module add apps/gromacs-DCU/2020.3/hpcx-v2.7.4-gcc-7.3.1
```

对应非module加载环境，只有source GMXRC脚本就可以自动配置运行环境，如

```
source $HOME/local/gromacs-2020.2/bin/GMXRC
```

其中`$HOME/local/gromacs-2020.2`为cmake执行时通过`-DCMAKE_INSTALL_PREFIX`指定的安装路径。

加载成功之后，推荐确认GROMACS的编译信息，以便对模拟数据有个整体把握。通过`gmx_mpi`命令的`-version`选项可获取编译信息： 

```
[xushun@login10 ~]$ gmx_mpi -quiet -version
      :-) GROMACS - gmx_mpi, 2020.3-dev-20210802-16d0bbb-dirty-unknown (-:

Executable:   /public/software/apps/gromacs-DCU/2020.3/hpcx-v2.7.4-gcc-7.3.1/bin/gmx_mpi
Data prefix:  /public/software/apps/gromacs-DCU/2020.3/hpcx-v2.7.4-gcc-7.3.1
Working dir:  /public/home/xushun
Command line:
  gmx_mpi -quiet -version

GROMACS version:    2020.3-dev-20210802-16d0bbb-dirty-unknown
GIT SHA1 hash:      16d0bbb851025e03e1dae9aaa35b021d5e53683e (dirty)
Branched from:      unknown
Precision:          single
Memory model:       64 bit
MPI library:        MPI
OpenMP support:     enabled (GMX_OPENMP_MAX_THREADS = 64)
GPU support:        (null)
SIMD instructions:  AVX2_256
FFT library:        fftw-3.3.8-sse2-avx_128_fma
RDTSCP usage:       enabled
TNG support:        enabled
Hwloc support:      disabled
Tracing support:    disabled
C compiler:         /opt/hpc/software/mpi/hpcx/v2.7.4/gcc-7.3.1/bin/mpicc GNU 7.3.1
C compiler flags:   -mavx2 -mfma -Wall -Wno-unused -Wunused-value -Wunused-parameter -Wextra -Wno-missing-field-initializers -Wno-sign-compare -Wpointer-arith -Wundef -fexcess-precision=fast -funroll-all-loops -Wno-array-bounds -O3 -DNDEBUG
C++ compiler:       /opt/hpc/software/mpi/hpcx/v2.7.4/gcc-7.3.1/bin/mpicxx GNU 7.3.1
C++ compiler flags: -mavx2 -mfma -Wall -Wextra -Wno-missing-field-initializers -Wpointer-arith -Wmissing-declarations -fexcess-precision=fast -funroll-all-loops -Wno-array-bounds -fopenmp -O3 -DNDEBUG
HIP compiler:      /public/software/compiler/rocm/dtk-21.04/bin/hipcc 4.0.20496-4f163c68
HIP compiler flags:
HIP driver:        0.4
HIP runtime:       19.61
```

从编译信息可知，此编译为单精度，支持OpenMP和MPI，使用hpcx 2.7.4 MPI编译器和DTK 21.04 HIP编译器。这里显示`GPU support: (null)`

在东方超算系统中，对比基于OpenCL的加速版本，相应的GROMACS的编译信息如下

```
[xushun@login10 examples]$ gmx_mpi -version -quiet
                   :-) GROMACS - gmx_mpi, 2020-UNCHECKED (-:

Executable:   /public/home/xushun/local/gromacs-2020/bin/gmx_mpi
Data prefix:  /public/home/xushun/local/gromacs-2020
Working dir:  /public/home/xushun/gromacs_dev/examples
Command line:
  gmx_mpi -version -quiet

GROMACS version:    2020-UNCHECKED
Build source could not be verified, because the checksum could not be computed.
Precision:          single
Memory model:       64 bit
MPI library:        MPI
OpenMP support:     enabled (GMX_OPENMP_MAX_THREADS = 64)
GPU support:        OpenCL
SIMD instructions:  AVX2_256
FFT library:        fftw-3.3.8-sse2-avx-avx2-avx2_128
RDTSCP usage:       enabled
TNG support:        enabled
Hwloc support:      hwloc-2.0.4
Tracing support:    disabled
C compiler:         /opt/rh/devtoolset-7/root/usr/bin/cc GNU 7.3.1
C compiler flags:   -mavx2 -mfma -pthread -fexcess-precision=fast -funroll-all-loops
C++ compiler:       /opt/rh/devtoolset-7/root/usr/bin/c++ GNU 7.3.1
C++ compiler flags: -mavx2 -mfma -pthread -fexcess-precision=fast -funroll-all-loops -fopenmp
OpenCL include dir: /opt/rocm/opencl/include
OpenCL library:     /opt/rocm/opencl/lib/x86_64/libOpenCL.so
OpenCL version:     2.0
```
可以看到`GPU support`和`Hwloc support`有不同。后者没有提供HIP信息，而只有OpenCL信息。

**Lysozyme in Water实例测试**

我们以Lysozyme in Water这个Tutorial为例 http://www.mdtutorials.com/gmx/lysozyme，测试相关命令是否可用。

```bash
wget https://files.rcsb.org/download/1AKI.pdb
grep -v HOH 1aki.pdb > 1AKI_clean.pdb  
gmx_mpi pdb2gmx -f 1AKI_clean.pdb -o 1AKI_processed.gro -water spce #input type 15
gmx_mpi editconf -f 1AKI_processed.gro -o 1AKI_newbox.gro -c -d 1.0 -bt cubic
gmx_mpi solvate -cp 1AKI_newbox.gro -cs spc216.gro -o 1AKI_solv.gro -p topol.top

wget http://www.mdtutorials.com/gmx/lysozyme/Files/ions.mdp
gmx_mpi grompp -f ions.mdp -c 1AKI_solv.gro -p topol.top -o ions.tpr
gmx_mpi genion -s ions.tpr -o 1AKI_solv_ions.gro -p topol.top -pname NA -nname CL -neutral #choose group 13 "SOL
gmx_mpi grompp -f minim.mdp -c 1AKI_solv_ions.gro -p topol.top -o em.tpr
srun -p normal -N 1 mpirun gmx_mpi mdrun -v -ntomp 31 -deffnm em
gmx_mpi energy -f em.edr -o potential.xvg

wget http://www.mdtutorials.com/gmx/lysozyme/Files/nvt.mdp
gmx_mpi grompp -f nvt.mdp -c em.gro -r em.gro -p topol.top -o nvt.tpr
srun -p normal -N 2 mpirun gmx_mpi mdrun -v -deffnm nvt
gmx_mpi energy -f nvt.edr -o temperature.xvg

wget http://www.mdtutorials.com/gmx/lysozyme/Files/npt.mdp
gmx_mpi grompp -f npt.mdp -c nvt.gro -r nvt.gro -t nvt.cpt -p topol.top -o npt.tpr
gmx_mpi mdrun -deffnm npt
gmx_mpi energy -f npt.edr -o pressure.xvg
gmx_mpi energy -f npt.edr -o density.xvg

wget http://www.mdtutorials.com/gmx/lysozyme/Files/md.mdp
gmx_mpi grompp -f md.mdp -c npt.gro -t npt.cpt -p topol.top -o run.tpr
```

查看性能

```
grep Core -A 4 lysozyme-9373168.log
```

mpirun使用`--bind-to none -report-bindings`可以提示20ns/day

在MPI多个task时，GPU和MPI rank之间需要成倍数关系，否则会有警告提示：

> There were 10 GPU tasks found on node h13r4n11, but 4 GPUs were available. If the GPUs are equivalent, then it is usually best to have a number of tasks that is a multiple of the number of GPUs. You should reconsider your GPU task assignment, number of ranks, or your use of the -nb, -pme, and -npme options, perhaps after measuring the performance you can get.



## 性能调优

GROMACS主要用于进行经典分子动力学模拟计算，原子之间的相互作用遵循牛顿定律。原子之间的相互作用包括成键力与非成键力，其中短程非成键力计算量最大且易于并行，长程力计算量也较大，且需要全局通信，难以并行。GROMACS官方介绍了[mdrun并行计算与GPU异构](https://manual.gromacs.org/current/user-guide/mdrun-performance.html)相关的运行配置。

对于易于并行的短程非成键作用力，GROMACS使用CUDA和OPENCL实现了在多种GPU上的加速。在没有可用GPU的情况下，还有多种CPU端向量化指令代码可供选用。

对于需要全局通信的长程非成键作用力，一般使用PME（Particle-Mesh-Ewald）算法进行计算。GROMACS 2019.3版本也添加了CUDA和OPENCL 版的PME代码，可以支持长程非成键作用力在单GPU卡上的计算。

本文档提供一个附件[examples](https://gitlab.com/orise/gromacs-hip/-/tree/master/examples)，提供Lysozyme-Water、Protein-Membrane、Water-OPLS-PME和Water-OPLS-RF等测试体系。针对东方超算系统Slurm作业调度系统，为每个体系提供了作业提交脚本run_slurm.sh，内含DCU加速运行模式的配置选项，相关命令操作如下

```bash
[xushun@login09 lysozyme_water]$ cat run_slurm.sh
#!/bin/bash
#SBATCH -J lysozyme
#SBATCH -p normal
#SBATCH -N 1
#SBATCH --ntasks-per-node=4
#SBATCH --cpus-per-task=4
#SBATCH --exclusive
#SBATCH --gres=dcu:4
#SBATCH -o %x-%j.log

env|grep SLURM
srun hostname |sort|awk 'NF==1{arr[$1]++} END{for (i in arr){printf "%s:%d\n",i,arr[i]}}'

export NAME_PREFIX=${SLURM_JOB_NAME}-${SLURM_JOBID}-${SLURM_NTASKS}n${SLURM_NNODES}N
export OMP_NUM_THREADS=${SLURM_CPUS_PER_TASK}

module purge
module add compiler/devtoolset/7.3.1
module add compiler/rocm/dtk/21.04
module add mpi/hpcx/2.7.4/gcc-7.3.1
module add apps/gromacs-DCU/2020.3/hpcx-v2.7.4-gcc-7.3.1

if [ ! -f run.tpr ] ; then
  gmx_mpi grompp -f md.mdp -c npt.gro -p topol.top -po md-out.mdp -o run.tpr
fi

date
mpirun -n ${SLURM_NTASKS} --bind-to core -report-bindings gmx_mpi mdrun -v  -dlb yes -pin on -deffnm ${NAME_PREFIX} -s run.tpr -nsteps 100000

rm -f \#*\#
#grep -E "Core|Atoms" -A 4 *-*-*.log
#sbatch this.sh
```

在examples目录提供了多个测试实例，每个实例目录中提供命令提交文件run_slurm.sh，根据超算环境需要做适配，特别是要调整MPI进程和OpenMP线程以及DCU的并行组合方式，使用如下命令提交

```
sbatch run_slurm.sh
```

也提供了使用单节点运行的脚本run_node_mpi.sh, run_note_tmpi.sh两个实例脚本。

所有任务运行完毕后，通过log日志提取性能数据，以列表方式给出各个实例的性能结果如：

| Case | atoms | nodes | MPI/node | CPUCore+DCU/MPI | ns/day |
| ---- | ------------------ | ----------- | ----- | ----- | ---- |
| Lysozyme-Water | 33876 | 4 | 8 | 4+1/2 | 169.141 |
| Lysozyme-Water | 33876 | 4 | 4 | 8+1 | 104.150 |
| Protein-Membrane | 81743 | 4 | 8 | 4+4 | 72.410 |
| Protein-Membrane | 81743 | 4 | 4 | 8+4 | 47.465 |
| Water-OPLS-PME   | 1536000 | 4     | 8        | 4+4          | 10.529  |
| Water-OPLS-PME   | 1536000 | 4     | 4        | 8+4          | 5.869 |
| Water-OPLS-RF | 1536000 | 4 | 8 | 4+4 | 27.691 |
| Water-OPLS-RF | 1536000 | 4 | 4 | 8+4 | 19.389 |

以上结果每次运行可能有小的差异。可以看出对应同样的计算节点资源，不同的并行组合方式，在计算性能上有很大差异。

在这里性能调优的可以从CPU和DCU的负载上去调整，如果CPU或DCU的负载太小则有资源浪费，如果负载太高则为计算瓶颈。因此为了性能提升有个好效果，一般控制负载在50-70%左右。

多次测试的最佳性能汇总如下：

| Case             | atoms   | ns/day  |
| ---------------- | ------- | ------- |
| Lysozyme-Water   | 33876   | 169.141 |
| Protein-Membrane | 81743   | 72.410  |
| Water-OPLS-PME   | 1536000 | 10.529  |
| Water-OPLS-RF    | 1536000 | 27.691  |

每个模拟使用4个节点并行计算，每个节点使用MPI多进程并行，每个MPI进程会使用节点上OpenMP多线程和调用DCU卡加速。具体的配置为8 MPI/node，4 DCU/node，4 OpenMP/MPI，2 MPI/DCU。

## 在V100 GPU节点上测试

在单节点上V100 GPU上测试

**Lysozyme实例**

使用单卡模拟

```
gmx mdrun -dlb yes -pin on -ntmpi 1 -deffnm lysozyme-23n1N -s run.tpr -ntomp 23 -nsteps 100000
```

Performance:  319.928  (ns/day)  0.075 (hour/ns)  其中`-ntomp 23`选项可以通过环境变量`export OMP_NUM_THREADS=23`传入。

使用双卡模拟

```
gmx mdrun -dlb yes -pin on -ntmpi 2 -deffnm lysozyme-6n1N -s run.tpr -ntomp 6 -nsteps 100000
```

Performance:   133.121 (ns/day)   0.180 (hour/ns)  ，这里-ntomp 6是GROMACS 日志输出推荐的参数。



**protein membrane实例**

使用单卡模拟

```
gmx mdrun -dlb yes -pin on -ntmpi 1 -deffnm water-pme23n1N -s pme.tpr -ntomp 23 -nsteps 20000
```

Performance:  113.065 (ns/day)  0.212  (hour/ns)  

使用双卡模拟

```
gmx mdrun -dlb yes -pin on -ntmpi 2 -deffnm promem-6n2N -s benchMEM.tpr -ntomp 6 -nsteps 30000
```

Performance:  50.763 (ns/day)  0.473  (hour/ns)  



**water opls实例**

使用GPU单卡模拟

```
gmx mdrun -dlb yes -pin on -ntmpi 1 -deffnm water-pme23n1N -s pme.tpr -ntomp 23 -nsteps 20000
```

Performance:  21.208 (ns/day)  1.132 (hour/ns)  

```
gmx mdrun -dlb yes -pin on -ntmpi 1 -deffnm water-rf23n1N -s rf.tpr -ntomp 23 -nsteps 20000
```

使用GPU单卡模块，大概70~80%的GPU使用率，Performance:  22.122 (ns/day)  1.085 (hour/ns)  。



使用GPU双卡模拟

```
gmx mdrun -dlb yes -pin on -ntmpi 2 -deffnm water-rf23n1N -s rf.tpr -ntomp 6 -nsteps 20000
```

实验GPU双卡模拟，每卡大概28~38%的GPU使用率，Performance:  20.896 (ns/day)  1.149 (hour/ns)  ，性能并没有提升。

```
gmx mdrun -dlb yes -pin on -ntmpi 2 -deffnm water-pme23n1N -s pme.tpr -ntomp 6 -nsteps 20000
```

Performance:  8.655 (ns/day)   2.773(hour/ns)



## 参考链接

- A free GROMACS benchmark set https://www.mpinat.mpg.de/grubmueller/bench
- GROMACS Tutorial-Lysozyme in Water http://www.mdtutorials.com/gmx/lysozyme/
- GROMACS水分子(spc)全原子力场(opls-aa)模拟 https://www.ngui.cc/el/1441619.html
- GROMACS 水模拟教程 http://kangsgo.cn/p/gromacs%E6%95%99%E7%A8%8B1-%E6%B0%B4
- Getting good performance from mdrun https://manual.gromacs.org/current/user-guide/mdrun-performance.html
- Creating Faster Molecular Dynamics Simulations with GROMACS 2020 https://developer.nvidia.com/blog/creating-faster-molecular-dynamics-simulations-with-gromacs-2020/

