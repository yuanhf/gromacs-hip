#!/bin/bash
#SBATCH -J promem
#SBATCH -p normal
#SBATCH -N 4 
#SBATCH --ntasks-per-node=8
#SBATCH --cpus-per-task=4
#SBATCH --exclusive
#SBATCH --gres=dcu:4
#SBATCH -o %x-%j.log 

env|grep SLURM
srun hostname |sort|awk 'NF==1{arr[$1]++} END{for (i in arr){printf "%s:%d\n",i,arr[i]}}'

export NAME_PREFIX=${SLURM_JOB_NAME}-${SLURM_JOBID}-${SLURM_NTASKS}n${SLURM_NNODES}N
export OMP_NUM_THREADS=${SLURM_CPUS_PER_TASK}

module purge
module add compiler/devtoolset/7.3.1
module add compiler/rocm/dtk/21.04
module add mpi/hpcx/2.7.4/gcc-7.3.1
module add apps/gromacs-DCU/2020.3/hpcx-v2.7.4-gcc-7.3.1


date
mpirun -n ${SLURM_NTASKS} --bind-to core -report-bindings gmx_mpi mdrun -v -dlb yes -pin on -deffnm ${NAME_PREFIX} -s benchMEM.tpr -nsteps 30000

rm -f \#*\#
#grep -E "Core|Atoms" -A 4 *-*-*.log
#sbatch this.sh
