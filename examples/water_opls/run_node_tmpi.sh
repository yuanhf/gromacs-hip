#!/bin/bash

export MPI_JOBID=$$
export MPI_NTASKS=1
export MPI_NNODES=1
export MPI_CPUS_PER_TASK=16
export NAME_PREFIX=water-${MPI_JOBID}-${MPI_NTASKS}n${MPI_NNODES}N
export OMP_NUM_THREADS=${MPI_CPUS_PER_TASK}

module add compiler/gcc/8.3.0
module add gromacs/2020.3_tmpi_cu04

if [ ! -f pme.tpr ] ; then
gmx grompp -f pme.mdp -c conf.gro -p topol.top -po pme-out.mdp -o pme.tpr
fi

if [ ! -f rf.tpr ] ; then
gmx grompp -f rf.mdp -c conf.gro -p topol.top -po rf-out.mdp -o rf.tpr
fi

date
gmx mdrun -v -dlb yes -pin on -ntmpi ${MPI_NTASKS} --deffnm ${NAME_PREFIX}-pme -s pme.tpr -nsteps 20000
gmx mdrun -v -dlb yes -pin on -ntmpi ${MPI_NTASKS} --deffnm ${NAME_PREFIX}-rf -s rf.tpr -nsteps 20000

rm -f \#*\#
#grep -E "Core|Atoms" -A 4 *-*-*.log

