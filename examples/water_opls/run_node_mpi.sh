#!/bin/bash

export MPI_JOBID=$$
export MPI_NTASKS=1
export MPI_NNODES=1
export MPI_CPUS_PER_TASK=16
export NAME_PREFIX=water-${MPI_JOBID}-${MPI_NTASKS}n${MPI_NNODES}N
export OMP_NUM_THREADS=${MPI_CPUS_PER_TASK}

module add compiler/openmpi/4.1.5_GNU8.3
module add gromacs/2020.3_mpi_cu04

if [ ! -f pme.tpr ] ; then
gmx_mpi grompp -f pme.mdp -c conf.gro -p topol.top -po pme-out.mdp -o pme.tpr
fi

if [ ! -f rf.tpr ] ; then
gmx_mpi grompp -f rf.mdp -c conf.gro -p topol.top -po rf-out.mdp -o rf.tpr
fi

date
mpirun -n ${MPI_NTASKS} --bind-to core -report-bindings gmx_mpi mdrun -v -dlb yes -pin on -deffnm ${NAME_PREFIX}-pme -s pme.tpr -nsteps 20000
mpirun -n ${MPI_NTASKS} --bind-to core -report-bindings gmx_mpi mdrun -v -dlb yes -pin on -deffnm ${NAME_PREFIX}-rf -s rf.tpr -nsteps 20000

rm -f \#*\#
#grep -E "Core|Atoms" -A 4 *-*-*.log
