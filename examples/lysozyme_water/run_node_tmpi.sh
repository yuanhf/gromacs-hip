#!/bin/bash

export MPI_JOBID=$$
export MPI_NTASKS=1
export MPI_NNODES=1
export MPI_CPUS_PER_TASK=16
export NAME_PREFIX=lysozyme-${MPI_JOBID}-${MPI_NTASKS}n${MPI_NNODES}N
export OMP_NUM_THREADS=${MPI_CPUS_PER_TASK}
#or add -ntomp=${MPI_CPUS_PER_TASK}

module add compiler/gcc/8.3.0
module add gromacs/2020.3_tmpi_cu04


if [ ! -f run.tpr ] ; then
  gmx grompp -f md.mdp -c npt.gro -p topol.top -po md-out.mdp -o run.tpr
fi

date
gmx mdrun -v -dlb yes -pin on -ntmpi ${MPI_NTASKS} -deffnm ${NAME_PREFIX} -s run.tpr -nsteps 100000

rm -f \#*\#
#grep -E "Core|Atoms" -A 4 *-*-*.log
